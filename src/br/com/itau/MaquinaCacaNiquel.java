package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class MaquinaCacaNiquel {
    private List<Slot> listaSlots;


    public MaquinaCacaNiquel(int qtdSlots) {
        listaSlots = new ArrayList<>();

        for(int i=0; i< qtdSlots; i++){
            this.listaSlots.add(new Slot());
        }
    }


    public int calcularPontuacao(){
        int pontuacao=0;
        for(Slot simbolo: listaSlots){
            pontuacao= pontuacao + simbolo.getSimbolo().pontos;
        }
        if(verificarElementosIguais()){
            return pontuacao*100;
        }else{
            return pontuacao;
        }
    }

    public void imprimirSlots (){
        for(Slot simbolo: listaSlots){
            System.out.print(simbolo.getSimbolo() + " - " + simbolo.getSimbolo().pontos + ",");
        }

        System.out.print("Pontuacao: " +calcularPontuacao());

    }

    public boolean verificarElementosIguais(){
        boolean saoIguais=true;

        for (int i =1; i<listaSlots.size(); i++){
            if(!listaSlots.get(i-1).getSimbolo().equals(listaSlots.get(i).getSimbolo())){
                return saoIguais=false;

            }
        }
        return saoIguais;
    }
}
