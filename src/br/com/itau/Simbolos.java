package br.com.itau;

public enum Simbolos {
        BANANA(10),
        FRAMBOESA(50),
        MOEDA(100),
        SETE(300);

        public int pontos;

        Simbolos(int pontos) {
                this.pontos = pontos;
        }

}
