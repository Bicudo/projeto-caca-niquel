package br.com.itau;

import java.util.Random;

public class Slot {
    //enum com tipos de slots
    private Simbolos simbolo;

    public Slot() {
        Random random = new Random();
        int valor =  random.nextInt(Simbolos.values().length);
        this.simbolo = Simbolos.values()[valor];
    }

    public Simbolos getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(Simbolos simbolo) {
        this.simbolo = simbolo;
    }
}
